#!/bin/bash
#### Setup logging
log="/tmp/init.log"
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>$log 2>&1

#### Install packages
yum -y update
yum -y install docker zsh

#### Configure skel files
echo 'alias ll="ls -l"' >> /etc/skel/.bashrc
echo 'alias la="ls -la"' >> /etc/skel/.bashrc
echo 'alias rmf="rm -rf"' >> /etc/skel/.bashrc
curl -sSL https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc > /etc/skel/.zshrc

#### User stuff
# Set default shell for new users created with useradd
sed -i 's/^SHELL=.*$/SHELL=\/bin\/zsh/' /etc/default/useradd

# Create some initial groups
INITIAL_GROUPS='docker,wheel,ssh-users,www'
for group in $(echo $INITIAL_GROUPS | tr ',' ' '); do
    groupadd $group
done


useradd -G "$INITIAL_GROUPS" -c 'Vegard Stuen' vegard

## Add public key of user to authorized_keys
V_PUB_KEY1="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDCfJ6zfqyQ+2emix3R9u1eKxiIwdfv7KhACdHM7yBOgLCNofhN7IM1TDE7+CSTTByQXL7KHnFY0uAQDXoS757yzH6X6LQNwQvriJpcLYSvSvohnMRyor+0K/mYHAkN6+x0xPZjGb25/CGNbVT8QQ9ZtdEL8fEb4b+6drdVvlwc162ncdkgKxNZ2Ko/nXVj9HB91WU8Gzb0fqTCUwLNObbWip/FiZUx/Q2r8tPtNBNN5Kdathf4e043i9+tt2NZf9H39HCaRCSZCOxq5gNE+3kPakzCxRSULBhRCbWdr9z99e6QfHEMvPXmvFg9hLOkpxuzHLycUujY4yN3PCDkyfHX vegard.stuen@gmail.com
"
V_PUB_KEY2="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCdGdLO/Joz3LFidgG0WwnPwAIRTNKZzYlMrs8oHX9o5ZGrRw6EUqk4RFDCErWGorXtE1lHVPc6XG08p9NroDmBF9rGth/N7HsfHJGvRHTwPL8nrHuG3MAEmD0zNMuIWFNblHtyPMy0jKgKLT4NLU64rbAruO4n1jMyoy5wLC1LUiew+x3yN25aLr4J1kZsi0t2lYvOjSqZ3uRIUkxtAjgK5qIvI2gIdqHIwkFs9sLiKC/y77zZ0xpsAVZK6BLTd9DV9uZzu/lNsVO1QmseiuJwf77P5uFJvmD4Cuj57Y/Uz7e0wwpM4PuYbwXnuctgzKTD5npnUgKXjNJmh+ogtglz vegard.stuen+manjatop@gmail.com
"

V_HOME="/home/vegard"
V_SSH="$V_HOME/.ssh"
V_AUTH_KEYS="$V_SSH/authorized_keys"
mkdir -p $V_SSH
touch $V_AUTH_KEYS
chown -R vegard:vegard $V_SSH
chmod 600 $V_AUTH_KEYS
chmod 700 $V_SSH
echo "$V_PUB_KEY1" >> $V_AUTH_KEYS
echo "$V_PUB_KEY2" >> $V_AUTH_KEYS
echo "Added public key for vegard to $V_AUTH_KEYS"

#### Start and enable services
systemctl enable docker
systemctl start docker

#### Add support for terminals
mkdir -p /usr/share/terminfo/r
curl -sSL -o "/usr/share/terminfo/r/rxvt-unicode-256color" "https://gitlab.com/vstuen/upcloud-init-scripts/raw/master/rxvt-unicode-256color"

echo done
